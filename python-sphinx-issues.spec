%global _empty_manifest_terminate_build 0
Name:		python-sphinx-issues
Version:	5.0.0
Release:	1
Summary:	A Sphinx extension for linking to your project's issue tracker
License:	MIT
URL:		https://github.com/sloria/sphinx-issues
Source0:	https://files.pythonhosted.org/packages/source/s/sphinx_issues/sphinx_issues-%{version}.tar.gz
BuildArch:	noarch

BuildRequires:  python3-pip
BuildRequires:  python3-flit-core
Requires:	python3-sphinx
Requires:	python3-pytest
Requires:	python3-flake8
Requires:	python3-pre-commit
Requires:	python3-tox
Requires:	python3-mock
Requires:	python3-flake8-bugbear
Requires:	python3-flake8
Requires:	python3-pre-commit
Requires:	python3-flake8-bugbear
Requires:	python3-pytest
Requires:	python3-mock

%description
A Sphinx extension for linking to your project’s issue tracker.
Includes roles for linking to issues, pull requests, user profiles, with built-in support for GitHub

%package -n python3-sphinx-issues
Summary:	A Sphinx extension for linking to your project's issue tracker
Provides:	python-sphinx-issues
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-sphinx-issues
A Sphinx extension for linking to your project’s issue tracker.
Includes roles for linking to issues, pull requests, user profiles, with built-in support for GitHub


%package help
Summary:	Development documents and examples for sphinx-issues
Provides:	python3-sphinx-issues-doc
%description help
A Sphinx extension for linking to your project’s issue tracker.
Includes roles for linking to issues, pull requests, user profiles, with built-in support for GitHub


%prep
%autosetup -n sphinx_issues-%{version}

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-sphinx-issues -f filelist.lst
%dir %{python3_sitelib}/*
%{python3_sitelib}/sphinx_issues/__pycache__/*.pyc

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Dec 03 2024 jinshuaiyu <jinshuaiyu@kylinos.cn> - 5.0.0-1
- Upgrade to version 5.0.0
- Drop Python 3.8 and support 3.13
- Silence 'already registered' warnings in tests
- Remove cwe and cve roles

* Fri Aug 16 2024 dongjiao <dongjiao@kylinos.cn> - 4.1.0-1
- Upgrade to version 4.1.0
  - Support Python 3.8-3.12.Older versions are no longer supported.

* Thu Jun 23 2022 SimpleUpdate Robot <tc@openeuler.org> - 3.0.1-1
- Upgrade to version 3.0.1

* Wed Nov 25 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
